import React, { Suspense, lazy } from "react";
import "./App.css";
import {
  Link,
  MakeGenerics,
  Outlet,
  ReactLocation,
  Router,
  useMatch,
} from "@tanstack/react-location";

const Home = lazy(() => import("./components/Home"));
const About = lazy(() => import("./components/About"));

const location = new ReactLocation();

function App() {
  const routes = [
    { path: "/", element: <Home /> },
    { path: "/about", element: <About /> },
  ];

  return (
    <div className="App">
      <Suspense fallback={<div>Loading...</div>}>
        <Router location={location} routes={routes}>
          <Link to="/" activeOptions={{ exact: true }}>
            Home
          </Link>
          <Link to="/about" activeOptions={{ exact: true }}>
            About
          </Link>
          <Outlet />
        </Router>
      </Suspense>
    </div>
  );
}

export default App;
